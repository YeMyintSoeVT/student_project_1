<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/secureadminpanel','Admin\UserController@loginForm');
Route::post('/login','Admin\UserController@login');

Route::group(['prefix' => 'admin'], function () {
    Route::get('/landing',function(){
        return view('adminpanel.landing.landing');
    });
    Route::resource('users', 'Admin\UserController');

    Route::get('/','Admin\Admin_Auth\LoginController@loginForm');
    Route::get('/login','Admin\Admin_Auth\LoginController@loginForm');
    Route::post('/login','Admin\Admin_Auth\LoginController@login');
    Route::post('/logout','Admin\Admin_Auth\LoginController@logout');

    Route::get('/register','Admin\Admin_Auth\RegisterController@registerForm');
    Route::post('/register','Admin\Admin_Auth\RegisterController@register');
});

//package routes
Route::resource('/admin/packages', 'Admin\PackageController');