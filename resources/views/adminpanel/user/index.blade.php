@extends('adminpanel.layout.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="text-primary text-center">User List</h4>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>
                                @if($user->roles)
                                @foreach($user->roles as $role)
                                - {{ucfirst($role->name)}}
                                @endforeach
                                @endif
                            </td>
                            <td>
                                <a href="{{url('admin/users/1/edit')}}" class="btn btn-success btn-sm"> Add Role </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
