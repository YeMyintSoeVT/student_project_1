@extends('adminpanel.layout.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="text-primary text-center">Package List</h4>
                <div class="row mb-2">
                    <div class="col-12">
                        <a href="/admin/packages/create" class="btn btn-primary float-right"><span style="font-size:20px;">+</span> Add</a>
                    </div>
                </div>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Package Type</th>
                            <th>Max Person</th>
                            <th>Description</th>
                            <th>Image</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($packages as $package)
                        <tr>
                            <td>{{$package->id}}</td>
                            <td>{{$package->package_type_id}}</td>
                            <td>{{$package->max_person}}</td>
                            <td>{{$package->description}}</td>
                            <td>
                            <img src="{{ $package->path()}}" alt="" width="100px" >
                            </td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
