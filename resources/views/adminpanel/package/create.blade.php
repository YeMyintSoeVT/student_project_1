@extends('adminpanel.layout.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row mb-4">
                    <div class="col-12">
                        <h4 class="text-primary text-center">Package Create</h4>
                    </div>
                </div>
                <div class="row">                    
                    <div class="col-12">

                        <form method="post" action="{{url('/admin/packages')}}" enctype="multipart/form-data">
                            @csrf
         
                            <div class="col-10 offset-1">

                                <div class="form-group row">
                                    <label for="package_type_id" class="col-sm-2 col-form-label">Type</label>
                                    <div class="col-sm-10">
                                        <select class="custom-select" id="package_type_id" name="package_type_id">
                                            <option selected>Open this select menu</option>
                                            @foreach ($packageTypes as $packageType)

                                                <option value="{{ $packageType->id }}">{{$packageType->name}}</option>

                                            @endforeach;
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="max_person" class="col-sm-2 col-form-label">Max Person</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="max_person" class="form-control" id="max_person">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="image" class="col-sm-2 col-form-label">Image</label>
                                    <div class="col-sm-10">
                                        <div class="custom-file">
                                            <input type="file" name="image">
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="description" class="col-sm-2 col-form-label">Description</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" id="description" name="description" rows="3" placeholder="Text ... "></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-10 mx-auto text-center">
                                        <button class="btn btn-success">Create</button>
                                    </div>
                                </div>

                            </div>                    
                        </form> 
                    </div>   
                </div>            
            </div>
        </div>
    </div>
@endsection