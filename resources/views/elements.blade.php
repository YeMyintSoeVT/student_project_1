@extends('layouts.master')
@section('content')
        <!-- Elements -->

        <div class="elements">
            <div class="container">
                <div class="row">
                    <div class="col">

                        <!-- Buttons -->
                        <div class="buttons">
                            <div class="elements_title">
                                <h2>Buttons</h2>
                            </div>
                            <div class="buttons_container d-flex flex-row align-items-start justify-content-start flex-wrap">
                                <div class="button button_1"><a href="#">Subscribe</a></div>
                                <div class="button button_2"><a href="#">Subscribe</a></div>
                                <div class="button button_3"><a href="#">Subscribe</a></div>
                                <div class="button button_4"><a href="#">Subscribe</a></div>
                            </div>
                        </div>

                        <!-- Accordions & Tabs -->
                        <div class="acc_tabs">
                            <div class="elements_title">
                                <h2>Accordions & Tabs</h2>
                            </div>
                            <div class="row acc_tabs_row">
                                <div class="col-lg-6">

                                    <!-- Accordions -->
                                    <div class="accordions">

                                        <!-- Accordion -->
                                        <div class="accordion_container">
                                            <div class="accordion d-flex flex-row align-items-center">
                                                <div>Vivamus auctor mi eget odio feugiat, quis aliquet velit ornare</div>
                                            </div>
                                            <div class="accordion_panel">
                                                <div>
                                                    <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse nec faucibus velit. Quisque eleifend orci ipsum, a bibendum lacus suscipit sit. Vestibulum ante ipsum
                                                        primis in faucibus.</p>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Accordion -->
                                        <div class="accordion_container">
                                            <div class="accordion d-flex flex-row align-items-center">
                                                <div>Auctor mi eget odio feugiat, quis aliquet velit ornare</div>
                                            </div>
                                            <div class="accordion_panel">
                                                <div>
                                                    <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse nec faucibus velit. Quisque eleifend orci ipsum, a bibendum lacus suscipit sit. Vestibulum ante ipsum
                                                        primis in faucibus.</p>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Accordion -->
                                        <div class="accordion_container">
                                            <div class="accordion d-flex flex-row align-items-center active">
                                                <div>Vivamus auctor mi eget odio feugiat, quis aliquet velit ornare</div>
                                            </div>
                                            <div class="accordion_panel">
                                                <div>
                                                    <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse nec faucibus velit. Quisque eleifend orci ipsum, a bibendum lacus suscipit sit. Vestibulum ante ipsum
                                                        primis in faucibus.</p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="col-lg-6 tabs_col">

                                    <!-- Tabs -->
                                    <div class="tabs">
                                        <div class="tabs_container">
                                            <div class="tabs d-flex flex-row align-items-center justify-content-start flex-wrap">
                                                <div class="tab">Vivamus auctor</div>
                                                <div class="tab">Auctor mi eget</div>
                                                <div class="tab active">Vivamus auctor</div>
                                            </div>
                                            <div class="tab_panels">
                                                <div class="tab_panel">
                                                    <div class="tab_panel_content">
                                                        <div class="tab_text">
                                                            <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse nec faucibus velit. Quisque eleifend orci ipsum, a bibendum lacus suscipit sit. Vestibulum
                                                                ante ipsum primis in faucibus.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab_panel">
                                                    <div class="tab_panel_content">
                                                        <div class="tab_text">
                                                            <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse nec faucibus velit. Quisque eleifend orci ipsum, a bibendum lacus suscipit sit. Vestibulum
                                                                ante ipsum primis in faucibus.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab_panel active">
                                                    <div class="tab_panel_content">
                                                        <div class="tab_text">
                                                            <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse nec faucibus velit. Quisque eleifend orci ipsum, a bibendum lacus suscipit sit. Vestibulum
                                                                ante ipsum primis in faucibus.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <!-- Loaders -->
                        <div class="loaders">
                            <div class="elements_title">
                                <h2>Loaders</h2>
                            </div>
                            <div class="loaders_container d-flex flex-row align-items-start justify-content-start flex-wrap">

                                <!-- Loader -->
                                <div class="loader_container text-center">
                                    <div class="loader text-center" data-perc="0.9">
                                        <div class="loader_content">
                                            <div class="loader_title">Good Services</div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Loader -->
                                <div class="loader_container text-center">
                                    <div class="loader text-center" data-perc="0.8">
                                        <div class="loader_content">
                                            <div class="loader_title">Tourists</div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Loader -->
                                <div class="loader_container text-center">
                                    <div class="loader text-center" data-perc="1.0">
                                        <div class="loader_content">
                                            <div class="loader_title">Satisfaction</div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Loader -->
                                <div class="loader_container text-center">
                                    <div class="loader text-center" data-perc="0.5">
                                        <div class="loader_content">
                                            <div class="loader_title">Children</div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- Milestones -->
                        <div class="milestones">
                            <div class="elements_title">
                                <h2>Milestones</h2>
                            </div>

                            <!-- Milestones -->
                            <div class="milestones_container d-flex flex-row align-items-start justify-content-start flex-wrap">

                                <!-- Milestone -->
                                <div class="milestone d-flex flex-row align-items-start justify-content-start">
                                    <div class="milestone_content">
                                        <div class="milestone_counter" data-end-value="45">0</div>
                                        <div class="milestone_title">Rooms available</div>
                                    </div>
                                </div>

                                <!-- Milestone -->
                                <div class="milestone d-flex flex-row align-items-start justify-content-start">
                                    <div class="milestone_content">
                                        <div class="milestone_counter" data-end-value="21" data-sign-after="K">0</div>
                                        <div class="milestone_title">Tourists this year</div>
                                    </div>
                                </div>

                                <!-- Milestone -->
                                <div class="milestone d-flex flex-row align-items-start justify-content-start">
                                    <div class="milestone_content">
                                        <div class="milestone_counter" data-end-value="2">0</div>
                                        <div class="milestone_title">Swimming pools</div>
                                    </div>
                                </div>

                                <!-- Milestone -->
                                <div class="milestone d-flex flex-row align-items-start justify-content-start">
                                    <div class="milestone_content">
                                        <div class="milestone_counter" data-end-value="143">0</div>
                                        <div class="milestone_title">Staff members</div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- Icon Boxes -->
                        <div class="icon_boxes">
                            <div class="elements_title">
                                <h2>Icon Boxes</h2>
                            </div>
                            <div class="row icon_boxes_row">

                                <!-- Icon Box -->
                                <div class="col-lg-4 icon_box_col">
                                    <div class="icon_box d-flex flex-column align-items-center justify-content-start text-center">
                                        <div class="icon_box_icon"><img src="images/icon_1.svg" class="svg" alt="https://www.flaticon.com/authors/monkik"></div>
                                        <div class="icon_box_title">
                                            <h2>Fabulous Resort</h2>
                                        </div>
                                        <div class="icon_box_text">
                                            <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse nec faucibus velit. Quisque eleifend orci ipsum, a bibendum.</p>
                                        </div>
                                    </div>
                                </div>

                                <!-- Icon Box -->
                                <div class="col-lg-4 icon_box_col">
                                    <div class="icon_box d-flex flex-column align-items-center justify-content-start text-center">
                                        <div class="icon_box_icon"><img src="images/icon_2.svg" class="svg" alt="https://www.flaticon.com/authors/monkik"></div>
                                        <div class="icon_box_title">
                                            <h2>Infinity Pool</h2>
                                        </div>
                                        <div class="icon_box_text">
                                            <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse nec faucibus velit. Quisque eleifend orci ipsum, a bibendum.</p>
                                        </div>
                                    </div>
                                </div>

                                <!-- Icon Box -->
                                <div class="col-lg-4 icon_box_col">
                                    <div class="icon_box d-flex flex-column align-items-center justify-content-start text-center">
                                        <div class="icon_box_icon"><img src="images/icon_3.svg" class="svg" alt="https://www.flaticon.com/authors/monkik"></div>
                                        <div class="icon_box_title">
                                            <h2>Luxury Rooms</h2>
                                        </div>
                                        <div class="icon_box_text">
                                            <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse nec faucibus velit. Quisque eleifend orci ipsum, a bibendum.</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
