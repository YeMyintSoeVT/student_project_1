<?php

namespace App\Http\Controllers\Admin\Admin_Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function loginForm(){
        return view('adminpanel.auth.login');
    }
    public function login(){
        request()->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        Auth::attempt(['email' => request()->email, 'password' => request()->password]);
        return redirect('admin/landing');
    }


    public function logout(){
        Auth::logout();
        return redirect('/');
    }
}
