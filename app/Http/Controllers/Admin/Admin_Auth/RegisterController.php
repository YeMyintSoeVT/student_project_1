<?php

namespace App\Http\Controllers\Admin\Admin_Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    public function registerForm(){
        return view('adminpanel.auth.register');
    }
    public function register(){
        $datas = request()->validate([
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|min:6',
            'confirmation_password' => 'required|same:password'
        ]);
        $datas['password'] = bcrypt(request()->password);
        $user = User::create($datas);
        Auth::login($user);
        return redirect('admin/landing');
    }
}
