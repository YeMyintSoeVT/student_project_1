<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use App\Package;
use App\PackageType;

class PackageController extends Controller
{
    public function index()
    {
        $packages = Package::all();
        return view('adminpanel.package.home', compact('packages'));
    }

    public function create()
    {
        $packageTypes = PackageType::all();
        return view('adminpanel.package.create', compact('packageTypes'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'package_type_id' => 'required',
            'max_person' => 'required',
            'image' => 'required', 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'required',
        ]);

        $package  = new package ;

        if($request->hasFile('image')){

            $image = $request->file('image');
            $name = date('YmdHis') . "." . $request->image->getClientOriginalExtension();
            $folder = 'public/images/packages_img/';
            $filePath = $folder . $name;

            $file = request()->file('image');
            
            $request->image->storeAs('public/images',$name);
          

            $package->image = $name;
           
            $package->package_type_id = $request->package_type_id;
            $package->max_person = $request->max_person;
            $package->description = $request->description;

            $package->save();
           
        };

        $package->package_type_id = $request->package_type_id;
        $package->max_person = $request->max_person;
        $package->description = $request->description;

        $package->save();

        return redirect('/admin/packages');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
