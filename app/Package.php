<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = ['package_type_id', 'max_person', 'description', 'image'];

    public function packageType()
    {
        return $this->belongsTo('App\PackageType');
    }

    public function path()
    {
        
        return '/storage/images/'.$this->image;
    }
}
