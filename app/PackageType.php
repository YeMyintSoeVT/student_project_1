<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageType extends Model
{
    protected $guarded = [];

    public function package ()
    {
        return $this->hasMany('App\Package');
    }
}
